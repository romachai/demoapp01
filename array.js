var data = ['A', 'B', 'C', 'D']
data.push('E')
console.log(data)

var x = data.pop()
console.log(data)

var y = data.shift()
console.log(data)

data.unshift('X')
console.log(data)

var data2 = ['A', 'B', 'C', 'D']

console.log((data2.splice(2, 1)))

console.log((data2.splice(1, 1)))

console.log((data2.splice(1, 5)))

console.log((data2.splice(2, 0, 'X')))

console.log((data2.splice(2, 1, 'X')))

console.log(data2.splice(data2.length, 0, 'E'))

console.log(data2.splice(data2.length, 1, 1))

console.log(data2.splice(0, 1))

console.log(data2.splice(0, 1))